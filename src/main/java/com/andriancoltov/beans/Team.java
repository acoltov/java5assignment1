package com.andriancoltov.beans;

import com.andriancoltov.interfaces.ITeam;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Objects;

/**
 * Created by Alienware on 2015-07-23.
 */


public class Team implements ITeam{

    private List<Player> playersList;
    private Trainer trainer;
    private String teamName;
    private int foundationYear;

    public Team(List<Player> playersList, Trainer trainer, String teamName, int foundationYear) {
        this.playersList = playersList;
        this.trainer = trainer;
        this.teamName = teamName;
        this.foundationYear = foundationYear;
    }

    public int getFoundationYear() {
        return foundationYear;
    }

    public void setFoundationYear(int foundationYear) {
        this.foundationYear = foundationYear;
    }

    public List<Player> getPlayersList() {
        return playersList;
    }

    public void setPlayersList(List<Player> playersList) {
        this.playersList = playersList;
    }

    public Trainer getTrainer() {
        return trainer;
    }

    public void setTrainer(Trainer trainer) {
        this.trainer = trainer;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Team)) return false;
        Team team = (Team) o;
        return Objects.equals(foundationYear, team.foundationYear) &&
                Objects.equals(playersList, team.playersList) &&
                Objects.equals(trainer, team.trainer) &&
                Objects.equals(teamName, team.teamName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(playersList, trainer, teamName, foundationYear);
    }

    @Override
    public String toString() {
        return "Team{" +
                "playersList=" + playersList +
                ", trainer=" + trainer +
                ", teamName='" + teamName + '\'' +
                ", foundationYear=" + foundationYear +
                '}';
    }
}

package com.andriancoltov.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Alienware on 2015-07-26.
 */

@Configuration
@ComponentScan(basePackages = "com.andriancoltov")
public class OrganisationConfig {
}

package com.andriancoltov.factory;

import com.andriancoltov.beans.*;
import com.andriancoltov.playerEnumerator.PlayerType;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Alienware on 2015-07-23.
 */

@Component
public class FactoryPlayer {

    private final Currency defaultCurrency = Currency.getInstance(Locale.US);
    private String firstName;
    private String lastName;
    private int age;
    private String countryOfBirth;
    private PlayerType position;
    private BigDecimal annualSalary;
    private Currency currency;
    private Statistics statistics;

    public Currency getDefaultCurrency() {
        return this.defaultCurrency;
    }

    public Player createPlayer(String firstName, String lastName, int age, String countryOfBirth, PlayerType position,
                               BigDecimal annualSalary, Currency currency, Statistics statistics) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.countryOfBirth = countryOfBirth;
        this.position = position;
        this.annualSalary = annualSalary;
        this.currency = currency;
        this.statistics = statistics;

        if (isValidPlayer()) {
            switch (position) {
                case goalkeeper:
                    return new PlayerGoalkeeper(this.firstName, this.lastName, this.age, this.countryOfBirth,
                            this.annualSalary, this.currency, this.statistics);
                case defender:
                    return new PlayerDefender(this.firstName, this.lastName, this.age, this.countryOfBirth,
                            this.annualSalary, this.currency, this.statistics);
                case midfielder:
                    return new PlayerMidfielder(this.firstName, this.lastName, this.age, this.countryOfBirth,
                            this.annualSalary, this.currency, this.statistics);
                case forward:
                    return new PlayerForward(this.firstName, this.lastName, this.age, this.countryOfBirth,
                            this.annualSalary, this.currency, this.statistics);
            }
        } else {
            System.out.println("Player details are incorrect so player not created \n");
        }
        return null;
    }

    //function to validate the player
    private boolean isValidPlayer() {
        if (isValidStr(firstName) && isValidStr(lastName) && isValidAge() && isValidStr(countryOfBirth)
                && isValidSalary() && isValidStatistics()) {
            return true;
        } else {
            System.out.println("Invalid player");
            return false;
        }
    }

    //function to validate the strings: firstName, lastName, countryOfBirth
    private boolean isValidStr(String str) {
        final Pattern strPattern = Pattern.compile("^[A-Za-z ]++$");

        if (strPattern.matcher(str).matches()) {
            return true;
        } else {
            System.out.println("Invalid Player firstName, lastName or countryOfBirth");
            return false;
        }
    }

    //function to validate the age
    private boolean isValidAge() {
        if (age >= 20 && age <= 23) {
            return true;
        } else {
            System.out.println("Invalid Player age");
            return false;
        }
    }

    //function to validate player position
//    private boolean isValidPosition(){
//        if (position.equalsIgnoreCase("goalkeeper") || position.equalsIgnoreCase("defender") ||
//                position.equalsIgnoreCase("midfielder") || position.equalsIgnoreCase("forward")) {
//            return true;
//        }
//        else {
//            System.out.println("Invalid Player position");
//            return false;
//        }
//    }

    //function to validate player salary
    private boolean isValidSalary() {
        if (!annualSalary.equals(null) && !annualSalary.equals(BigDecimal.ZERO) && annualSalary.compareTo(BigDecimal.ZERO) > 0) {
            return true;
        } else {
            System.out.println("Invalid Player salary");
            return false;
        }
    }

    //function to validate player statistics
    private boolean isValidStatistics() {
        if (statistics != null && statistics.getNumberOfGoals() >= 0 && statistics.getNumberOfBookings() >= 0) {
            return true;
        } else {
            System.out.println("Invalid Player statistics");
            return false;
        }
    }

}

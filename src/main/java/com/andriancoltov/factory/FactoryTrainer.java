package com.andriancoltov.factory;

import com.andriancoltov.beans.Trainer;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;
import java.util.regex.Pattern;

/**
 * Created by Alienware on 2015-07-23.
 */

@Component
public class FactoryTrainer {

    private final Currency defaultCurrency = Currency.getInstance(Locale.US);
    private String firstName;
    private String lastName;
    private int age;
    private BigDecimal annualSalary;
    private Currency currency;

    public Currency getDefaultCurrency() {
        return this.defaultCurrency;
    }

    public Trainer createTrainer(String firstName, String lastName, int age, BigDecimal annualSalary, Currency currency) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.annualSalary = annualSalary;
        this.currency = currency;

        if (isValidTrainer()) {
            return new Trainer(this.firstName, this.lastName, this.age, this.annualSalary, this.currency);
        } else {
            System.out.println("Trainer details are incorrect so trainer not created\n");
            return null;
        }
    }

    //function to validate the trainer
    private boolean isValidTrainer() {
        if (isValidStr(firstName) && isValidStr(lastName) && isValidAge() &&
                isValidSalary()) {
            return true;
        } else {
            System.out.println("Invalid trainer");
            return false;
        }
    }

    //function to validate the strings: firstName, lastName
    private boolean isValidStr(String str) {
        final Pattern strPattern = Pattern.compile("^[A-Za-z ]++$");

        if (strPattern.matcher(str).matches()) {
            return true;
        } else {
            System.out.println("Invalid trainer firstName or lastName");
            return false;
        }
    }

    //function to validate the age
    private boolean isValidAge() {
        if (age >= 40) {
            return true;
        } else {
            System.out.println("Invalid trainer age");
            return false;
        }
    }

    //function to validate salary
    private boolean isValidSalary() {
        if (!annualSalary.equals(null) && !annualSalary.equals(BigDecimal.ZERO) && annualSalary.compareTo(BigDecimal.ZERO) > 0) {
            return true;
        } else {
            System.out.println("Invalid trainer salary");
            return false;
        }
    }

}

package com.andriancoltov.interfaces;

/**
 * Created by Alienware on 2015-07-27.
 */
public interface IStatistics {

    int numberOfGoals = 0;
    int numberOfBookings = 0;
}

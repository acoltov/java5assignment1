package com.andriancoltov;

import com.andriancoltov.beans.*;
import com.andriancoltov.config.OrganisationConfig;
import com.andriancoltov.factory.FactoryPlayer;
import com.andriancoltov.playerEnumerator.PlayerType;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.util.Currency;
import java.util.Locale;

/**
 * Created by Alienware on 2015-07-26.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {OrganisationConfig.class})
public class PlayerTest {

    private static String validFirstName;
    private static String validLastName;
    private static int validAge;
    private static String validCountryOfBirth;
    private static PlayerType validPosition;
    private static BigDecimal validAnnualSalary;
    private static Currency validCurrency;
    private static Statistics validStatistics;

    @BeforeClass
    public static void prepData() {
        validFirstName = "John";
        validLastName = "Smith";
        validAge = 22;
        validCountryOfBirth = "Canada";
        validPosition = PlayerType.defender;
        validAnnualSalary = new BigDecimal("3000000");
        validCurrency = Currency.getInstance(Locale.US);
        validStatistics = new Statistics(3,3);
    }

    @Autowired
    private FactoryPlayer factoryPlayer;

    @Test
    public void FactoryPlayerNotNull() {
        assertNotNull(factoryPlayer);
    }

    @Test
    public void createValidPlayer() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, validCountryOfBirth, validPosition, validAnnualSalary, validCurrency, validStatistics);
        assertNotNull(player);
    }

    @Test
    public void createInvalidPlayerWrongFirstNameEmpty() {
        Player player = factoryPlayer.createPlayer("", validLastName, validAge, validCountryOfBirth, validPosition, validAnnualSalary, validCurrency, validStatistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerWrongFirstNameNumbers() {
        Player player = factoryPlayer.createPlayer("test 5", validLastName, validAge, validCountryOfBirth, validPosition, validAnnualSalary, validCurrency, validStatistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerWrongLastNameEmpty() {
        Player player = factoryPlayer.createPlayer(validFirstName, "", validAge, validCountryOfBirth, validPosition, validAnnualSalary, validCurrency, validStatistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerWrongLastNameNumbers() {
        Player player = factoryPlayer.createPlayer(validFirstName, "3test", validAge, validCountryOfBirth, validPosition, validAnnualSalary, validCurrency, validStatistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerWrongAgeLess() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, 19, validCountryOfBirth, validPosition, validAnnualSalary, validCurrency, validStatistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerWrongAgeMore() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, 24, validCountryOfBirth, validPosition, validAnnualSalary, validCurrency, validStatistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerWrongCountryOfBirthEmpty() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, "", validPosition, validAnnualSalary, validCurrency, validStatistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerWrongCountryOfBirthNonLetter() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, "Can@ada", validPosition, validAnnualSalary, validCurrency, validStatistics);
        assertNull(player);
    }

    @Test
    public void checkPlayerDefender() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, validCountryOfBirth, PlayerType.defender, validAnnualSalary, validCurrency, validStatistics);
        assertThat(PlayerType.defender.toString(), is(player.getPosition().toString()));
    }

    @Test
    public void checkPlayerForward() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, validCountryOfBirth, PlayerType.forward, validAnnualSalary, validCurrency, validStatistics);
        assertThat(PlayerType.forward.toString(), is(player.getPosition().toString()));
    }

    @Test
    public void checkPlayerGoalkeeper() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, validCountryOfBirth, PlayerType.goalkeeper, validAnnualSalary, validCurrency, validStatistics);
        assertThat(PlayerType.goalkeeper.toString(), is(player.getPosition().toString()));
    }

    @Test
    public void checkPlayerMidfielder() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, validCountryOfBirth, PlayerType.midfielder, validAnnualSalary, validCurrency, validStatistics);
        assertThat(PlayerType.midfielder.toString(), is(player.getPosition().toString()));
    }


    @Test
    public void createInvalidPlayerWrongAnnualSalary() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, validCountryOfBirth, validPosition, new BigDecimal("-1"), validCurrency, validStatistics);
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerWrongStatisticsGoals() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, validCountryOfBirth, validPosition, validAnnualSalary, validCurrency, new Statistics(-1,0));
        assertNull(player);
    }

    @Test
    public void createInvalidPlayerWrongStatisticsBookings() {
        Player player = factoryPlayer.createPlayer(validFirstName, validLastName, validAge, validCountryOfBirth, validPosition, validAnnualSalary, validCurrency, new Statistics(1,-1));
        assertNull(player);
    }
}

package com.andriancoltov;

import com.andriancoltov.beans.*;
import com.andriancoltov.config.OrganisationConfig;
import com.andriancoltov.factory.FactoryPlayer;
import com.andriancoltov.factory.FactoryTeam;
import com.andriancoltov.factory.FactoryTrainer;
import com.andriancoltov.playerEnumerator.PlayerType;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Locale;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

/**
 * Created by Alienware on 2015-07-26.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {OrganisationConfig.class})
public class TeamTest {

    private static String validPlayerFirstName  = "Super";
    private static String validPlayerLastName = "Man";
    private static int validPlayerAge = 22;
    private static String validPlayerCountryOfBirth = "Canada";
    private static PlayerType validPlayerPositionDefender = PlayerType.defender;
    private static PlayerType validPlayerPositionForward = PlayerType.forward;
    private static PlayerType validPlayerPositionGoalkeeper = PlayerType.goalkeeper;
    private static PlayerType validPlayerPositionMidfielder = PlayerType.midfielder;
    private static BigDecimal validPlayerAnnualSalary = new BigDecimal("3000000");
    private static Currency validPlayerCurrency = Currency.getInstance(Locale.US);
    private static Statistics validPlayerStatistics = new Statistics(3,3);


    private static String validTrainerFirstName = "John";
    private static String validTrainerLastName = "Smith";
    private static int validTrainerAge = 40;
    private static BigDecimal validTrainerAnnualSalary = new BigDecimal("3000000");
    private static Currency validTrainerCurrency = Currency.getInstance(Locale.US);

    private static Player validPlayer;
    private static List<Player> validPlayersList;
    private static Trainer validTrainer;
    private static String validTeamName;
    private static int validFoundationYear;

    @BeforeClass
    public static void prepData() {
        validTeamName = "Canadiens";
        validFoundationYear = 1950;
    }

    @Autowired
    private FactoryTeam factoryTeam;

    @Autowired
    private FactoryTrainer factoryTrainer;

    @Autowired
    private FactoryPlayer factoryPlayer;

    @Test
    public void FactoryTeamNotNull() {
        assertNotNull(factoryTeam);
    }

    @Test
    public void FactoryTrainerNotNull() {
        assertNotNull(factoryTrainer);
    }

    @Test
    public void FactoryPlayerNotNull() {
        assertNotNull(factoryPlayer);
    }

    @Test
    public void createValidTeam() {
        validTrainer = factoryTrainer.createTrainer(validTrainerFirstName, validTrainerLastName, validTrainerAge, validTrainerAnnualSalary, validTrainerCurrency);
        validPlayer = factoryPlayer.createPlayer(validPlayerFirstName, validPlayerLastName, validPlayerAge, validPlayerCountryOfBirth, validPlayerPositionDefender, validPlayerAnnualSalary, validPlayerCurrency, validPlayerStatistics);
        validPlayersList = new ArrayList<Player>();
        for (int count = 1; count <=22; count++) {
            validPlayersList.add(validPlayer);
        }
        Team team = factoryTeam.createTeam(validPlayersList, validTrainer, validTeamName, validFoundationYear);

        assertNotNull(team);
    }

    @Test
    public void createInvalidTeamWrongNumberOfPlayersMore() {
        validTrainer = factoryTrainer.createTrainer(validTrainerFirstName, validTrainerLastName, validTrainerAge, validTrainerAnnualSalary, validTrainerCurrency);
        validPlayer = factoryPlayer.createPlayer(validPlayerFirstName, validPlayerLastName, validPlayerAge, validPlayerCountryOfBirth, validPlayerPositionDefender, validPlayerAnnualSalary, validPlayerCurrency, validPlayerStatistics);
        validPlayersList = new ArrayList<Player>();
        for (int count = 1; count <=23; count++) {
            validPlayersList.add(validPlayer);
        }
        Team team = factoryTeam.createTeam(validPlayersList, validTrainer, validTeamName, validFoundationYear);

        assertNull(team);
    }

    @Test
    public void createInvalidTeamWrongNumberOfPlayersLess() {
        validTrainer = factoryTrainer.createTrainer(validTrainerFirstName, validTrainerLastName, validTrainerAge, validTrainerAnnualSalary, validTrainerCurrency);
        validPlayer = factoryPlayer.createPlayer(validPlayerFirstName, validPlayerLastName, validPlayerAge, validPlayerCountryOfBirth, validPlayerPositionDefender, validPlayerAnnualSalary, validPlayerCurrency, validPlayerStatistics);
        validPlayersList = new ArrayList<Player>();
        for (int count = 1; count <=21; count++) {
            validPlayersList.add(validPlayer);
        }
        Team team = factoryTeam.createTeam(validPlayersList, validTrainer, validTeamName, validFoundationYear);

        assertNull(team);
    }

    @Test
    public void createInvalidTeamWrongTrainer() {
        validTrainer = factoryTrainer.createTrainer(validTrainerFirstName, validTrainerLastName, 39, validTrainerAnnualSalary, validTrainerCurrency);
        validPlayer = factoryPlayer.createPlayer(validPlayerFirstName, validPlayerLastName, validPlayerAge, validPlayerCountryOfBirth, validPlayerPositionDefender, validPlayerAnnualSalary, validPlayerCurrency, validPlayerStatistics);
        validPlayersList = new ArrayList<Player>();
        for (int count = 1; count <=22; count++) {
            validPlayersList.add(validPlayer);
        }
        Team team = factoryTeam.createTeam(validPlayersList, validTrainer, validTeamName, validFoundationYear);

        assertNull(team);
    }

    @Test
    public void createInvalidTeamWrongTeamName() {
        validTrainer = factoryTrainer.createTrainer(validTrainerFirstName, validTrainerLastName, validTrainerAge, validTrainerAnnualSalary, validTrainerCurrency);
        validPlayer = factoryPlayer.createPlayer(validPlayerFirstName, validPlayerLastName, validPlayerAge, validPlayerCountryOfBirth, validPlayerPositionDefender, validPlayerAnnualSalary, validPlayerCurrency, validPlayerStatistics);
        validPlayersList = new ArrayList<Player>();
        for (int count = 1; count <=22; count++) {
            validPlayersList.add(validPlayer);
        }
        Team team = factoryTeam.createTeam(validPlayersList, validTrainer, "", validFoundationYear);

        assertNull(team);
    }

    @Test
    public void createInvalidTeamWrongFoundationYear() {
        validTrainer = factoryTrainer.createTrainer(validTrainerFirstName, validTrainerLastName, validTrainerAge, validTrainerAnnualSalary, validTrainerCurrency);
        validPlayer = factoryPlayer.createPlayer(validPlayerFirstName, validPlayerLastName, validPlayerAge, validPlayerCountryOfBirth, validPlayerPositionDefender, validPlayerAnnualSalary, validPlayerCurrency, validPlayerStatistics);
        validPlayersList = new ArrayList<Player>();
        for (int count = 1; count <=22; count++) {
            validPlayersList.add(validPlayer);
        }
        Team team = factoryTeam.createTeam(validPlayersList, validTrainer, validTeamName, 1949);

        assertNull(team);
    }
}
